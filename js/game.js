window.onload = function() {
    var gameStartButton = document.getElementById("submit");
    gameStartButton.onclick = function() {
        // hide pregame area
        document.getElementById("pregame-area").style.zIndex = "-1";
    // DEBUG window at top left if true
        var DEBUG = true;

        if (DEBUG) {
            gameArea = document.getElementById("game-area");
            Debug = document.createElement("div");
            Debug.id = "debug";
            Debug.style.width = "200";
            Debug.style.height = "150";
            Debug.style.top = "0";
            Debug.style.left = "0";
            Debug.style.backgroundColor = "#ddeedd";
            Debug.style.position = "relative";
            Debug.style.zIndex = 300;
            gameArea.insertBefore(Debug, gameArea.firstChild);
        }

        cvs = document.getElementById("cvs");
        ctx = cvs.getContext("2d");
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = window.innerHeight;

        moveDirection = {
          north: "false",
          south: "false",
          west: "false",
          east: "false"
        };

    // player position, held client side as reflection of server
        pos = {
          x: 5000,
          y: 5000
        };

    // Initialize socket.io on client side
        socket = io.connect('http://localhost:3000', {'force new connection': true});

    // Tell server we are ready to be initialized with name entered in textbox
        socket.on("getClientUsername", function(data){
            var playerName = document.getElementById("username").value;
            socket.emit("sendClientUsername", {"name": playerName})
        });

    // Get unique client ID and store client-side after connecting to server
        socket.on('clientID', function (data) {
            localStorage.setItem("clientID", data);

            // Change client position as reflection of server state
            // Note that "data" var contains all information about all users
            socket.on("gameTick", function(data) {
                // data contains array of users and associated info. see app.js
                // store game entities to draw on next game tick
                entities = data
                myClientID = localStorage.getItem("clientID")
                for (var i=0; i<data.length; i++) {
                    if (data[i]["id"] == myClientID) {
                        pos.x = data[i].position["x"];
                        pos.y = data[i].position["y"];
                    }
                }
            })
        })



    // Handle key presses for movement and ability use; send to server
        window.onkeydown = function(e){
            var keyPressed = e.keyCode;
            e.preventDefault();
    // Handle movement
            updateMovementByClientID = "updateMovement" + localStorage.getItem("clientID")
            useAbilityByClientID = "clientUseAbility" + localStorage.getItem("clientID")

            if(keyPressed === 37){
                moveDirection.west = "true";
                socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : true, "moveDirection":"west"});
            }
            if(keyPressed === 38) {
                moveDirection.north = "true";
                socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : true, "moveDirection":"north"});
            }
            if(keyPressed === 39) {
                moveDirection.east = "true";
                socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : true, "moveDirection":"east"});
            }
            if(keyPressed === 40) {
                moveDirection.south = "true";
                socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : true, "moveDirection":"south"});
            }
    // Handle ability use
            if (keyPressed === 49) {
                abilityNumberUsed = "One";
                socket.emit(useAbilityByClientID, {"clientID":localStorage.getItem("clientID"), "abilityNumberUsed":abilityNumberUsed});
            }
            if (keyPressed === 50) {
                abilityNumberUsed = "Two";
                socket.emit(useAbilityByClientID, {"clientID":localStorage.getItem("clientID"), "abilityNumberUsed":abilityNumberUsed});
            }
            if (keyPressed === 51) {
                abilityNumberUsed = "Three";
                socket.emit(useAbilityByClientID, {"clientID":localStorage.getItem("clientID"), "abilityNumberUsed":abilityNumberUsed});
            }
            if (keyPressed === 52) {
                abilityNumberUsed = "Four";
                socket.emit(useAbilityByClientID, {"clientID":localStorage.getItem("clientID"), "abilityNumberUsed":abilityNumberUsed});
            }
         };
    // Stop moving in direction of keypress when key is released
        window.onkeyup = function(e){
             updateMovementByClientID = "updateMovement" + localStorage.getItem("clientID")
             var keyReleased = e.keyCode;
             e.preventDefault();

             if(keyReleased === 37){
                 moveDirection.west = "false";
                 socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : false, "moveDirection":"west"});
             }
             if(keyReleased === 38) {
                 moveDirection.north = "false";
                 socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : false, "moveDirection":"north"});
             }
             if(keyReleased === 39) {
                 moveDirection.east = "false";
                 socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : false, "moveDirection":"east"});
             }
             if(keyReleased === 40) {
                 moveDirection.south = "false";
                 socket.emit(updateMovementByClientID, {"clientID":localStorage.getItem("clientID"), "toggleMovementOn" : false, "moveDirection":"south"});
             }
        };

    // Resize the canvas to fill screen when window is resized
        window.onresize = resizeWindow;

    // Initialize main game loop
        setInterval(gameTick, 33);
    }
}

function gameTick() {
// Clear canvas to redraw
    cvs = document.getElementById("cvs");
    ctx = cvs.getContext("2d");
    ctx.clearRect(0, 0, 5000, 5000);
// Get relative x- and y-offset, and draw the background/game entities
    drawBackground(ctx, pos.x % 50, pos.y % 50);
    drawEntities(ctx, localStorage.getItem("clientID"));
// TODO: Draw objects
    // TODO: Handle game logic (collisions, hits) client side
}

function resizeWindow() {
    cvs = document.getElementById("cvs");
    cvs.width = window.innerWidth;
    cvs.height = window.innerHeight;
};

function drawBackground(ctx, xOffset, yOffset) {
    for (var i = 1; i <= 100; i++) {
        // Draw vertical lines
        ctx.beginPath();
        // in x-direction, canvas moves opposite to direction of movement, hence minus
        ctx.moveTo(50*i - 50 - xOffset,0);
        ctx.lineTo(50*i - 50 - xOffset,5000);
        ctx.stroke();
        // Draw horizontal lines
        ctx.beginPath();
        ctx.moveTo(0, 50*i - 50 + yOffset);
        ctx.lineTo(5000, 50*i - 50 + yOffset);
        ctx.stroke();
    }
    DEBUG = true;
    if (DEBUG) {
        document.getElementById("debug").innerHTML = "x: " + pos.x + "\n" + "y: " + pos.y
    }
}

function drawEntities(ctx, myClientID) {
    // draw OTHER players within page width/2 and page height/2 -- client player is fixed in center of screen
    // data[i] is individual entity sent by server: another player, a projectile, etc
    for (var i=0; i<entities.length; i++) {
        if (entities[i].id != myClientID) {
            // currently only drawing other players
            if (entityInDrawableRange(entities[i])) {
                drawEntity(ctx, entities[i], "player")
            }
        }
    }

    // draw abilities within page width/2 and page height/2

    // draw map obstacles within page width/2 and page height/2
}

// Check if player is "worth drawing" on canvas for player to see
// That is: is the player within pixel bounds of the screen plus or minus a bit?
function entityInDrawableRange(entity) {
    playerDistance = torusDistance(entity.position["x"], pos.x, entity.position["y"], pos.y)
    if (playerDistance[0] < window.innerWidth + 200 && playerDistance[1] < window.innerHeight + 200) {
        return true;
    }
    else {
        return false;
    }
}

// torus ≅ 2d grid with sides connected
function torusDistance(x1, x2, y1, y2) {
    xDistance = Math.min(Math.abs(x1 - x2), 10000 - Math.abs(x1 - x2))
    yDistance = Math.min(Math.abs(y1 - y2), 10000 - Math.abs(y1 - y2))
    return [xDistance, yDistance]
}

function drawEntity(ctx, entity, entityType) {
    if (entityType == "player") {
        relPlayerPosition = [window.innerWidth/2, window.innerHeight/2]
        // note: signs are reversed in y direction as 0,0 is top left of screen, not bottom left
        absDrawLocation = [entity.position["x"] - pos.x + relPlayerPosition[0], -entity.position["y"] + pos.y + relPlayerPosition[1]]
        cvs = document.getElementById("cvs");
        ctx = cvs.getContext("2d");
        ctx.beginPath();
        ctx.arc(absDrawLocation[0], absDrawLocation[1], 20, 0, 2*Math.PI);
        ctx.fillStyle = "green";
        ctx.fill();
        ctx.strokeStyle = "#003300";
        // draw code here
        ctx.stroke();
    }
}
