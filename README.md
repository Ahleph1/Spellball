# Overview

Web game using Node.js and socket.io for real-time communication. Big work-in-progress: nowhere near completion.

# Setting up to run locally

```git clone``` the repo locally, then run ```npm install``` in **spellball/**.

```npm start``` hosts the app on http://localhost:3000.

# Controls

Move with arrow keys.

1, 2, 3, 4 are abilities not yet implemented. In their place, they currently serve as additional movement keys.
