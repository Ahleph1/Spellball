var http = require('http');
var express = require('express');
var app = express();
var io = require('socket.io')(http);
var path = require('path');
var assert = require('assert');
// Express routing

// Set root directory for application
app.use(express.static('.'));

// Render index.html in browser
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

// all users connected
var users = []
// increment after adding user
var clientID = 0;


// On connection, send message
io.sockets.on('connection', function (socket) {
    // Add just-connected player's info to database and send the client their ID
    clientID += 1;

    // listen for player connection request
    socket.on("sendClientUsername", function(data) {
        newUser = new CreateUser(clientID, data["name"], 5000, 5000);
        socket.emit("clientID", clientID)
        users.push(newUser);
    });

    // Set socket connection strings to handle individual updates to movement and ability use by player
    updateMovementByClientID = "updateMovement" + clientID;
    useAbilityByClientID = "clientUseAbility" + clientID;

    socket.on(updateMovementByClientID, function(data) {
        for (var i=0; i<users.length; i++){
            if (users[i].id == data["clientID"] && data["toggleMovementOn"] == true) {
                users[i].moveDirection[data["moveDirection"]] = true;
                break;
            } else if (users[i].id == data["clientID"] && data["toggleMovementOn"] == false) {
                users[i].moveDirection[data["moveDirection"]] = false;
                break;
            }
        }
    })

    socket.on(useAbilityByClientID, function(data) {
        for (var i=0; i<users.length; i++) {
            if (users[i].id == data["clientID"]) {
                users[i].useAbility["ability" + data["abilityNumberUsed"]] = true;
            }
        }
    })

    // send client request for inputted username
    socket.emit("getClientUsername", "");

// Handle server-side movement
    // on new connection, stop old gameticks interval to start new one
    try {
        clearInterval(gameTicks)
    }
    catch(err) {
        null
    }
    gameTicks = setInterval(gameTick, 33, socket, users);
});

// Initialize socket.io on server side

io.listen(app.listen(3000));

function gameTick(socket, users) {
    if (users.length > 0) {
        users = updateAllUserPositions(users);
        users = usePlayerAbilities(users);
        for (var i=0; i<users.length; i++) {
            io.emit("gameTick", users)
        }
    }
}

var updateAllUserPositions = function(users) {
    for (var i=0; i<users.length; i++) {
        users[i] = updatePosition(users[i]);
    }
    return users;
}

var usePlayerAbilities = function(users) {
    for (var i=0; i<users.length; i++) {
        // handle ability one used
        if (users[i].useAbility["abilityOne"] == true) {
            console.log(users[i])
            users[i].position["x"] += 20;
            users[i].useAbility["abilityOne"] = false;
        }
        // handle ability two used
        if (users[i].useAbility["abilityTwo"] == true) {
            console.log(users[i])
            users[i].position["x"] -= 20;
            users[i].useAbility["abilityTwo"] = false;
        }
        // handle ability three used
        if (users[i].useAbility["abilityThree"] == true) {
            console.log(users[i])
            users[i].position["y"] += 20;
            users[i].useAbility["abilityThree"] = false;
        }
        //handle ability four used
        if (users[i].useAbility["abilityFour"] == true) {
            console.log(users[i])
            users[i].position["y"] -= 20;
            users[i].useAbility["abilityFour"] = false;
        }
    }
    return users
}

// "user" is an element of "users"
// handle arrow-key movement. map is a torus (boundaries are connected) hence modulus operator usage
function updatePosition(user) {
    if (user.moveDirection["north"] == true) {
        user.position["y"] = (user.position["y"] + 10) % 10000;
        console.log(user.position["y"])
    }
    if (user.moveDirection["south"] == true) {
        if (user.position["y"] < 10) {
            user.position["y"] = 10000 + (user.position["y"] - 10);
        }
        if (user.position["y"] >= 10) {
            user.position["y"] = user.position["y"] - 10;
        }
        console.log(user.position["y"])
    }
    if (user.moveDirection["east"] == true) {
        user.position["x"] = (user.position["x"] + 10) % 10000;
        console.log(user.position["x"])
    }
    if (user.moveDirection["west"] == true) {
        if (user.position["x"] < 10) {
            user.position["x"] = 10000 + (user.position["x"] - 10);
        }
        if (user.position["x"] >= 10) {
            user.position["x"] = user.position["x"] - 10;
        }
        console.log(user.position["x"])
    }
    return user
}

function CreateUser(id, username, startPositionX, startPositionY) {
    this.id = id;
    this.username = username;
    this.position = {
        "x" : startPositionX,
        "y" : startPositionY
    };
    this.abilities = {
        "abilityOne" : "shoot",
        "abilityTwo" : "dash",
        "abilityThree" : "nova",
        "abilityFour" : "shield"
    }
    // set initial dates far enough into the past so abilities can be used immediately
    this.abilityTimers = {
        "abilityOneTimer" : Date.now() - 5000000,
        "abilityTwoTimer" : Date.now() - 5000000,
        "abilityThreeTimer" : Date.now() - 5000000,
        "abilityFourTimer" : Date.now() - 5000000,
    }
    this.health = 300;
    this.moveDirection = {
        "north" : false,
        "south" : false,
        "east" : false,
        "west" : false
    };
    this.useAbility = {
        "abilityOne" : false,
        "abilityTwo" : false,
        "abilityThree" : false,
        "abilityFour" : false
    }
}
